import PySimpleGUI as psg

psg.theme("DarkAmber") # немного цвета

# всё, что внутри окна
layout = [ [psg.Text('какой-то текст в поле 1')],
            [psg.Text('Введите текст в поле 2'), psg.InputText()],
            [psg.Button('Ok'), psg.Button('отмена')]    ]
            
# создадим окно
window = psg.Window('Заголовок', layout)
# Цикл чтобы получить значения из поля ввода
while True:
    event, values = window.read()
    if event == psg.WIN_CLOSED or event == 'отмена': # если закрыли окно ил нажали отмену
        break
    print('Вы ввели ', values[0])   # номер поля, из которого выведет значение (как в массиве от 0 до N 
    
window.close()
