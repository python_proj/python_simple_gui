# КРАТКОЕ ОПИСАНИЕ МОДУЛЯ
# Модуль для создания БД,
#  внесения записей в БД и показа их
#
import PySimpleGUI as psg
import sqlite3
from sqlite3 import Error
#import os.path
import pathlib
import my_module_SQLite_logs as dmod
import my_module_logs_write as logs

#form1 = psg.FlexForm('Первая простая форма', location=(300,500))

lay = [ [psg.Text('Вводите данные по заявке')],
        [psg.Text('Номер: ', size=(15,1)), psg.InputText()],
        [psg.Text('Кто помогал: ', size=(15,1)), psg.InputText(size = (30,1))],
        [psg.Text('Что сделали: ', size=(15,1)), psg.InputText(size = (65,1))],
        [psg.Submit('OK', button_color=('white', 'springgreen4'), key='sub'), 
         psg.Cancel('отмена', button_color=('white', 'firebrick3'), key='cans'),
         psg.Button('тест', button_color=('white', 'blue'), key='test1'),
         psg.Button('Проверить и создать БД', button_color=('black', 'white'), key='test_db'),
         psg.Button('Внести данные в БД', button_color=('blue', 'white'), key='insert_db'),
         psg.Button('Показать данные в БД', button_color=('blue', 'white'), key='select_db')],
        [psg.Output(size=(88,20))]
        ]

# функция для получения значений из полей ввода
def get_values(val1,val2,val3):
    # тестовый Принт - для проверки полученных значений
    print("первое: " + val1 + ", " + "второе: " + val2 + ", " + "третье: " + val3)

#Устанавливаем соединение с базой
def sql_conn():
	try:
		conn = sqlite3.connect("files/test_db1.db")
		return conn
		print('Соединение успешно!')
	except Error:
		print(Error)

# проверка и создание БД
def con_sql(connect):
    try:
        #conn = sqlite3.connect("test_db1.db")
        cur = connect.cursor()
# создание таблицы
        cur.execute("""CREATE TABLE IF NOT EXISTS incidents
                        (_id INTEGER PRIMARY KEY,
                        inc_num text, helper text, todo text)
                        """)
        print("БД успешно создана")
    except Error:
        print(Error)
    finally:
        connect.close()
        


# попытка внесения записи в Таблицу
def insert_sql(val1, val2, val3):
    # какая-то переменная
    #arr1 = [('первый','второй', 'третий')]
    arr_vals = [(val1, val2, val3)]
    #print (arr_vals)   
    try:
            connect = sqlite3.connect("files/test_db1.db")    # " + val1 + " ," + val2 + " ," + val3 + "
            cur = connect.cursor()
            #cur.execute("""INSERT INTO incidents(inc_num, helper, todo) VALUES ( """ + val1 + " ," + val2 + " ," + val3 + " )""")
            cur.executemany("INSERT INTO incidents(inc_num, helper, todo) VALUES (?,?,? )", arr_vals)
            connect.commit() # без этой сраной строки ничего не запишется
            print("Данные успешно внесены")
    except Error:
            print(Error)
    finally:
            connect.close()
 

# показать записи в БД
def sql_select():
        try:
                connect = sqlite3.connect("files/test_db1.db")
                cur = connect.cursor()
                sql = "SELECT * FROM incidents"
                #sql1 = "SELECT * FROM artists WHERE name=?"
                cur.execute(sql)
                print(cur.fetchall())
                #cur.execute(sql1, [("Rammstein")])
                print(cur.fetchall())
        except Error:
                print(str(Error) + " не удалось показать")
        finally:
                cur.close()

# создадиим окно
window = psg.Window('Внесение инфы в БД через ГУИ. Версия 1.5', lay, default_element_size = (20,2), text_justification='r',
    auto_size_text=False, default_button_element_size=(12,1))

window.Finalize()
#window['test1'].update(disabled=True)

# поймаем события и значения
while True:
    events, values = window.read()
    print(events)
    
    if events == psg.WIN_CLOSED or events == 'cans':    # выход или закрытие
        exit(69)    # диалог о подтверждении закрытия
        #break      # просто завершить
        
    if events == 'sub':
        window['test1'].update(disabled=False)
        
    elif events == 'test1':
        #dmod.sql_create(dmod.sql_con())
        path1 = pathlib.Path("files/test_db1.db")
        if not path1.exists():
            print ("БД не существует. Создайте её.")
            psg.popup('БД не существует!!','Создайте её.')
        else:
            print("БД есть")
            
        #get_values(values[0], values[1], values[2])
        # заносим значения в массив, чтобы потом выгрузить в БД
        #arr_vals = [values[0], values[1], values[2]]
        #for i in arr_vals:
        #    print (i)
    elif events == 'test_db':
        dmod.sql_create(dmod.sql_con())
    elif events == 'select_db':
        dmod.sql_select(dmod.sql_con())
        #sql_select()
# проверка на заполнение полей - НЕ работает!!!
    elif events == 'insert_db':
        str0 = values[0]
        str1 = values[1]
        str2 = values[2]
        # проверка на сущестовование файла с БД
        path1 = pathlib.Path("files/test_db1.db")
        if not path1.exists():
            print("БД не существует. Создайте её.")
            psg.popup('БД не существует!!','Создайте её.')           
        else:
            if (str0 != '') and (str1 != '') and (str2 != ''):            
                dmod.sql_insert(dmod.sql_con(), str0, str1, str2)
            else:           
                psg.popup('Не все значения присутствуют!!','' ) 
                logs.log_write("Не все поля заполнены!")
                logs.log_write('----------------------------')

