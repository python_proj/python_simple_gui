# КРАТКОЕ ОПИСАНИЕ МОДУЛЯ
# Модуль реализации функций для работы с SQLIte
import sqlite3 as lite
from sqlite3 import Error as e
import my_module_logs_write as logs

    
                                # connect to db
def sql_con():
    try:
        conn = lite.connect('files/test_db1.db')
        logs.log_write('Соединение успешно!')
        return conn
    except e:
        logs.log_write("Соединение не удалось: " + str(e))
        
# экземпляр подключения к БД по пути из текстового поля
def sql_con_path(path):
    try:
        conn = lite.connect(path)
        logs.log_write('Соединение успешно к БД по пути из текстового поля!')
        return conn
    except e:
        print("Соединение не удалось к БД по пути из текстового поля!:  " + str(e))
            
                                # проверка и создание БД
def sql_create(connect):
    try:
        cur = connect.cursor()
                                # создание таблицы
        cur.execute("""CREATE TABLE IF NOT EXISTS incidents
                        (_id INTEGER PRIMARY KEY,
                        inc_num text, helper text, todo text)
                        """)
        logs.log_write("БД успешно создана \n")
    except e:
        logs.log_write("Создать БД не удалось: " + str(e))
    finally:
        connect.close()
            
                                # попытка внесения записи в Таблицу
def sql_insert(connect, val1, val2, val3):
        arr_vals = [(val1, val2, val3)]
        try:
            cur = connect.cursor()
            cur.executemany("INSERT INTO incidents(inc_num, helper, todo) VALUES (?,?,? )", arr_vals)
            connect.commit() # без этой сраной строки ничего не запишется
            logs.log_write("Данные успешно внесены \n")
        except e:
            logs.log_write(e)
        finally:
            connect.close()

                                # показать записи в БД
def sql_select(connect):
    try:
        cur = connect.cursor()
        sql = "SELECT * FROM incidents"
        cur.execute(sql)
        print(cur.fetchall())
    except e:
        logs.log_write(str(e) + " не удалось показать \n")
    finally:
        cur.close()            

# получить имя Таблицы из БД
def sql_get_table_name(connect):
    cur = connect.cursor()
    cur.execute('SELECT name from sqlite_master WHERE TYPE = "table"')
    tablename = cur.fetchall()
    return str(tablename)