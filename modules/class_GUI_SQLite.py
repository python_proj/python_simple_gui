# Класс реализации функций для работы с SQLIte
import sqlite3
from sqlite3 import Error
import my_logs_write


class DbModule(self):
    print("db go!")
    
                                # connect to db
    def sql_con():
        try:
            conn = sqlite3.connect('test_db1.db')
            return conn
            Logs.log_write('Соединение успешно!')
        except Error:
            Logs.log_write("Соединение не удалось: " + str(Error))
            
                                # проверка и создание БД
    def sql_create(connect):
        try:
        #conn = sqlite3.connect("test_db1.db")
            cur = connect.cursor()
                                # создание таблицы
            cur.execute("""CREATE TABLE IF NOT EXISTS incidents
                        (_id INTEGER PRIMARY KEY,
                        inc_num text, helper text, todo text)
                        """)
            Logs.log_write("БД успешно создана")
        except Error:
            Logs.log_write("Создать БД не удалось: " + str(Error))
        finally:
            connect.close()
            
                                # попытка внесения записи в Таблицу
    def sql_insert(val1, val2, val3):
        # какая-то переменная
        #arr1 = [('первый','второй', 'третий')]
        arr_vals = [(val1, val2, val3)]
        #print (arr_vals)
        try:
            connect = sqlite3.connect("test_db1.db")    # " + val1 + " ," + val2 + " ," + val3 + "
            cur = connect.cursor()
            #cur.execute("""INSERT INTO incidents(inc_num, helper, todo) VALUES ( """ + val1 + " ," + val2 + " ," + val3 + " )""")
            cur.executemany("INSERT INTO incidents(inc_num, helper, todo) VALUES (?,?,? )", arr_vals)
            connect.commit() # без этой сраной строки ничего не запишется
            print("Данные успешно внесены")
        except Error:
            print(Error)
        finally:
            connect.close()

                                # показать записи в БД
    def sql_select():
            try:
                connect = sqlite3.connect("test_db1.db")
                cur = connect.cursor()
                sql = "SELECT * FROM incidents"
                #sql1 = "SELECT * FROM artists WHERE name=?"
                cur.execute(sql)
                print(cur.fetchall())
                #cur.execute(sql1, [("Rammstein")])
                print(cur.fetchall())
            except Error:
                print(str(Error) + " не удалось показать")
            finally:
                cur.close()            

