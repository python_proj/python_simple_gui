# КРАТКОЕ ОПИСАНИЕ МОДУЛЯ
# Модуль выгрузки данных таблицы из базы данных
# и записи их в эксель файл
#
import pandas as pd
#from pandas import Errors
import PySimpleGUI as psg
import os
#import sqlite3 as lite
import my_module_SQLite_logs as db
import my_module_logs_write as logs
#
# --------------------------------------------------------------рисуем интерфейс
#
lay = [ [psg.Text("БД для импорта:                "),
         psg.InputText(size=(40,1), tooltip='   путь к файлу БД   ', key='-in1-'),
         psg.FileBrowse(file_types=(('SQLite3 DB Files','*.db'),))],
        [psg.Text("Имя папки для сохранения"),
         psg.In(size=(40,1), enable_events=True, tooltip='   путь сохранения файла .xlsx   ', key='-folder-'),
         psg.FolderBrowse()],
        [psg.HorizontalSeparator(color='white')],
        [psg.Button('Показать данные из БД', button_color=('white','blue'), tooltip='   показать содержимое таблицы в БД   ', key='btn_from_db'),
         psg.Button(" ТЕСТО ", button_color=('white','springgreen4'), tooltip='   Кнопка для тестов функций   ',
                    key='btn_test1'),
         psg.Button("сохранить в *.Exlx", button_color=('white','firebrick'), tooltip='   выгружает инфу из Бд и записывает в excel   ',
                    #disabled=True,
                    key='btn_to_excel')],
         [psg.Output(size=(70,25))]     ]
#
# -----------------------------------------------------переменные для проверки

# ---------------------------------------------------------функции и процедуры
#
# экземпляр подключения к БД ----- пока что не используется
"""def sql_con(path):
    try:
        conn = lite.connect(path)
        print("Соединение успешно!")
        return(conn)
    except e:
        print("Соединение не удалось!:  " + str(e))"""
# глобальный пустой ДатаФрейм для заполнения его данными из БД и выгрузки иx в Эксель  
df2 = pd.DataFrame()
# функция загрузки данных из БД в Дата фрейм
def df_upload(connect, df1):
    try:        
# экземпляр ДатаФрейм с содержанием выполненного запроса
        #conn = lite.connect(path) # получаем объект коннекта к БД из функции sql_con
        #print("Соединение успешно!")
        df2 = pd.read_sql("SELECT * FROM incidents", connect)
        logs.log_write("Данные загружены успешно!")
        #return df2
        print (df2)
    except ImportError:
        logs.log_write("Данные не выгружены:  " + str(ImportError))
        
# функция загрузки данных из БД в Дата фрейм
def df_upload1(connect):
    try:        
# экземпляр ДатаФрейм с содержанием выполненного запроса
        #conn = lite.connect(path) # получаем объект коннекта к БД из функции sql_con
        df1 = pd.DataFrame()
        df1 = pd.read_sql("SELECT * FROM incidents", connect)
        logs.log_write("Данные загружены успешно!")
        return df1
    except ImportError:
        logs.log_write("Данные не выгружены:  " + str(ImportError))

#

# функция выгрузки в excel из Дата фрейма
def df_to_excel(filename, df):
    try:
        df.to_excel(filename, sheet_name='Выгрузка из БД')
        logs.log_write("Информация записана в файл")
    except ImportError:
        logs.log_write("Ошибка выгрузки:  " + str(ImportError))

# получить имя Таблицы из БД
"""def sql_get_table_name(conn):
    cur = conn.cursor()
    cur.execute('SELECT name from sqlite_master WHERE TYPE = "table"')
    tablename = cur.fetchall()
    print(tablename)"""

 
# создадим окно----------------------------------------------------------------------
window = psg.Window('Сохранить данные из БД в excel', lay, default_element_size=(20,1), 
    text_justification='r', auto_size_text='False', default_button_element_size=(13,1))

# этот параметр вызывается в случае, если перед построением окна нужно сначала
# взаимодействовать с элементами как в коде ниже: update(dis...)
#window.Finalize()
#window['to_xlsx'].update(disabled=True)

# работа с событиями и значениями на форме
while True:
    events, values = window.read()
    path_db = ''
    path_excel = ''
    
    if events == psg.WIN_CLOSED:    #----------------------------------------------------
        break
    if events == 'btn_test1':
        #if values['-in1-']
        conn = db.sql_con_path(values['-in1-'])
        table_name = db.sql_get_table_name(db.sql_get_table_name(conn))
        print("-----------------------------")
        print(df2)
        """path_db = values['-folder-']
        path_excel = values['-in1-']
        if (path_db != '') and (path_excel != ''):
            psg.popup(' Всё на месте ', path_db + '  ' + path_excel )    #
        else:
            psg.popup('Не все значения присутствуют!!', path_db + '  ' + path_excel ) 
            print("Не все значения присутствуют!")
            print('----------------------------')"""
        #Esql_con(values['-in1-'])
        #text_input = values['-in1-']
        #psg.popup('Всё получилось успешно!', text_input)    #
        #print('----------------------------')
    if events == 'btn_from_db':
        # Проверка на заполнение пути к БД
        if (values['-in1-'] != ''):
            #df_upload1(db.sql_con_path(values['-in1-']))
            df_upload(db.sql_con_path(values['-in1-']), df2)
            #window['btn_to_excel'].update(disabled=False)
            #print(df1)
            print("Успешно!")
            logs.log_write('----------------------------')
        else:
            psg.popup('База данных не выбрана!!','' ) 
            logs.log_write("База данных не выбрана!")
            logs.log_write('----------------------------')
    if events == 'btn_to_excel':
        path_db = values['-folder-']
        path_excel = values['-in1-']
        if (path_db != '') and (path_excel != ''):
                        # выгружает инфу из Бд и записывает в excel - сразу все jgthfwbb
            path_db = path_db + '/' + 'test1.xlsx'
            #print(path_db)
            #df_to_excel(path_db, df2)
            df_to_excel(path_db, df_upload1(db.sql_con_path(values['-in1-'])))
            print("Успешно!")
            logs.log_write('----------------------------')
        else:
            psg.popup('Не все значения присутствуют!!', path_db + '  ' + path_excel ) 
            logs.log_write("Не все значения присутствуют!")
            logs.log_write('----------------------------')
        
        
