import pandas as pd
import PySimpleGUI as psg
import os
import sqlite3 as lite
#
# рисуем интерфейс--------------------------------------------------------------
#
lay = [ [psg.Text("БД для импорта:                "),
         psg.InputText(size=(40,1), tooltip='   путь к файлу БД   ', key='-in1-'),
         psg.FileBrowse(file_types=(('SQLite3 DB Files','*.db'),))],
        [psg.Text("Имя файла для сохранения"),
         psg.In(size=(40,1), enable_events=True, tooltip='   путь сохранения файла .xlsx   ', key='-folder-'),
         psg.FolderBrowse()],
        [psg.HorizontalSeparator(color='white')],
        [psg.Button('выгрузить из БД', button_color=('white','blue'), tooltip='   показать содержимое таблицы в БД   ', key='btn_from_db'),
         psg.Button(" ТЕСТО ", button_color=('white','springgreen4'), tooltip='   Кнопка для тестов функций   ',
                    key='btn_test1'),
         psg.Button("сохранить в *.Exlx", button_color=('white','firebrick'), tooltip='   выгружает инфу из Бд и записывает в excel   ',
                    #disabled=True,
                    key='btn_to_excel')],
         [psg.Output(size=(70,25))]     ]
#
# переменные для проверки-----------------------------------------------------

# функции и процедуры---------------------------------------------------------
#
# экземпляр подключения к БД ----- пока что не используется
def sql_con(path):
    try:
        conn = lite.connect(path)
        print("Соединение успешно!")
        return(conn)
    except e:
        print("Соединение не удалось!:  " + str(e))
#
df1 = pd.DataFrame()
# функция загрузки данных из БД в Дата фрейм
def df_upload(path, df1):
    try:        
# экземпляр ДатаФрейм с содержанием выполненного запроса
        conn = lite.connect(path) # получаем объект коннекта к БД из функции sql_con
        print("Соединение успешно!")
        df1 = pd.read_sql("SELECT * FROM incidents", conn)
        print("Данные загружены успешно!")
        print(df1)
        return df1
    except e:
        print("Данные не выгружены:  " + str(e))

#

# функция выгрузки в excel из Дата фрейма
def df_to_excel(filename, df1):
    try:
        #con = sql_con(values[0]) 
        #df2 = df_upload(sql_con(values['-in1-']))
        df1.to_excel(filename, sheet_name='Выгрузка из БД')
        print("Информация записана в файл")
    except e:
        print("Ошибка выгрузки:  " + str(e))

# получить имя Таблицы из БД
def sql_get_table_name(conn):
    cur = conn.cursor()
    cur.execute('SELECT name from sqlite_master WHERE TYPE = "table"')
    tablename = cur.fetchall()
    print(tablename)
    #return tablename

 
# создадим окно----------------------------------------------------------------------
window = psg.Window('Сохранить данные из БД в excel', lay, default_element_size=(20,1), 
    text_justification='r', auto_size_text='False', default_button_element_size=(13,1))

# этот параметр вызывается в случае, если перед построением окна нужно сначала
# взаимодействовать с элементами как в коде ниже: update(dis...)
#window.Finalize()
#window['to_xlsx'].update(disabled=True)

# работа с событиями и значениями на форме
while True:
    events, values = window.read()
    path_db = ''
    path_excel = ''
    
    if events == psg.WIN_CLOSED:    #----------------------------------------------------
        break
    if events == 'btn_test1':
        conn = sql_con(values['-in1-'])
        sql_get_table_name(conn)
        """path_db = values['-folder-']
        path_excel = values['-in1-']
        if (path_db != '') and (path_excel != ''):
            psg.popup(' Всё на месте ', path_db + '  ' + path_excel )    #
        else:
            psg.popup('Не все значения присутствуют!!', path_db + '  ' + path_excel ) 
            print("Не все значения присутствуют!")
            print('----------------------------')"""
        #Esql_con(values['-in1-'])
        #text_input = values['-in1-']
        #psg.popup('Всё получилось успешно!', text_input)    #
        #print('----------------------------')
    if events == 'btn_from_db':
        #con = sql_con(values['-in1-'])
        df_upload(values['-in1-'], df1)
        window['btn_to_excel'].update(disabled=False)
        print('----------------------------')
    if events == 'btn_to_excel':
        path_db = values['-folder-']
        path_excel = values['-in1-']
        if (path_db != '') and (path_excel != ''):
                        # выгружает инфу из Бд и записывает в excel - chfpe dct jgthfwbb
            path_db = path_db + '/test1.xlsx'
            print(path_db)
            df_to_excel(path_db, df_upload(values['-in1-'], df1))
            print('----------------------------')
        else:
            psg.popup('Не все значения присутствуют!!', path_db + '  ' + path_excel ) 
            print("Не все значения присутствуют!")
            print('----------------------------')
        
        
