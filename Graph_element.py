import PySimpleGUI as psg

#---------------
lay = [ [psg.Graph(canvas_size=(500,500),
                   graph_bottom_left=(-100,-100),
                   graph_top_right=(100,100),
                   enable_events=True)],
        [psg.Text("БД для импорта:                ")] ]

# создадим окно----------------------------------------------------------------------
window = psg.Window('Сохранить данные из БД в excel', lay, default_element_size=(20,1), 
    text_justification='r', auto_size_text='False', default_button_element_size=(13,1))

# работа с событиями и значениями на форме
while True:
    events, values = window.read()
    if events == psg.WIN_CLOSED:
        break
