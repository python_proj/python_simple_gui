import pandas as pd
import os
import sqlite3 as lite

path = os.getcwd() + '\\files\\'
# проверка на существование директории
#if not os.chdir(path):
#    os.mkdir("\\files\\")
#    print("новая директория создана")
#else:
#    print("директория уже есть")

os.chdir(path)
    
print(path + "\\files\\")
#os.chdir("C:\PythonProj\PysimpleGUI\files\")

#file = 'from_sqlite.xlsx'

# создать пустой лист
#dfl = [] 

# создать пустой ДатаФрейм
#dfs = pd.DataFrame()

# экземпляр подключения к БД 
conn = lite.connect("test_db1.db")

# экземпляр ДатаФрейм с содержанием выполненного запроса
df1 = pd.read_sql("SELECT  * FROM incidents", conn, index_col = '_id') # первичный ключ
# индексация несколькими столбцами
# df1 = pd.read_sql("SELECT  * FROM incidents", conn, index_col = ['_id', 'inc_name']) 

print (df1)

try:
    df1.to_excel('dataFrame_from_sqlite.xlsx', sheet_name='Выгрузка из SQLite3')
    print("Информация записана")
except e:
    print(e)
'''
try:
    with pd.ExcelWriter('dataFrame_from_sqlite.xlsx', sheet_name='Выгрузка из SQLite3') as writer:
        df1.to_excel(writer)
        print("Информация записана")
except e:
    print(e)
'''

