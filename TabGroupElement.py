import PySimpleGUI as psg
import sqlite3
from sqlite3 import Error
import pandas as pd

#--------------------------РИСУЕМ ИНТЕРФЕЙС-------------------------------------
#
# Слой на первой вкладке
tab_lay1 = [ [psg.Text("БД для импорта:                "),
             psg.InputText(size=(40,1), tooltip='   путь к файлу БД   ', key='-in1-'),
             psg.FileBrowse(file_types=(('SQLite3 DB Files','*.db'),))],
            [psg.Text("Имя файла для сохранения"),
             psg.In(size=(40,1), enable_events=True, tooltip='   путь сохранения файла .xlsx   ', key='-folder-'),
             psg.FolderBrowse()],
            [psg.HorizontalSeparator(color='white')],
            [psg.Button('выгрузить из БД', button_color=('white','blue'), tooltip='   показать содержимое таблицы в БД   ', key='btn_from_db'),
             psg.Button(" ТЕСТО ", button_color=('white','springgreen4'), tooltip='   Кнопка для тестов функций   ',
                    key='btn_test1'),
             psg.Button("сохранить в *.Exlx", button_color=('white','firebrick'), tooltip='   выгружает инфу из Бд и записывает в excel   ',
                    #disabled=True,
                    key='btn_to_excel')],
             [psg.Output(size=(70,25))]
             ]
# Слой на второй вкладке    
tab_lay2 = [ [psg.Text('Вводите данные по заявке')],
            [psg.Text('Номер заявки: ', size=(15,1)), psg.InputText()],
            [psg.Text('Кто помогал: ', size=(15,1)), psg.InputText(size = (30,1))],
            [psg.Text('Что сделали: ', size=(15,1)), psg.InputText(size = (65,1))],
            [psg.HorizontalSeparator(color='white')],
            [psg.Submit('OK', button_color=('white', 'springgreen4'), key='sub'), 
             psg.Cancel('отмена', button_color=('white', 'firebrick3'), key='cans'),
             psg.Button('тест', button_color=('white', 'blue'), key='test1'),
             psg.Button('Проверить БД', button_color=('black', 'white'), key='test_db'),
             psg.Button('Внести данные в БД', button_color=('blue', 'white'), key='insert_db'),
             psg.Button('Показать данные в БД', button_color=('blue', 'white'), key='select_db')],
            [psg.Output(size=(88,20))]
            ]   
# сами Табы и присвоенные им слои
tabs = [    [psg.Tab('Выгрузить данные из БД', tab_lay1, key='tab1'), 
                            psg.Tab('Новая запись в БД', tab_lay2, key='tab2'),
                     ]    ]
# общий слой с размещённой на нём ТабГруп - для группировки Табов (tabs)
layout = [  [psg.TabGroup(tabs, enable_events=True,
                          key='-TABGROUP-')],
            [psg.Text('Tab number'), psg.In(key='-in1-', size=(5,1)), psg.B('Invisible')]    ]

#
#-------------------------------ПРОЦЕДУРЫ и ФУНКЦИИ---------------------------------------
# Для работы с БД SQLite3

# Для ТАБ 1
# экземпляр подключения к БД ----- пока что не используется
def sql_con(path):
    try:
        conn = lite.connect(path)
        print("Соединение успешно!")
        return(conn)
    except e:
        print("Соединение не удалось!:  " + str(e))
#
df1 = pd.DataFrame()
# функция загрузки данных из БД в Дата фрейм
def df_upload(path, df1):
    try:        
# экземпляр ДатаФрейм с содержанием выполненного запроса
        conn = lite.connect(path) # получаем объект коннекта к БД из функции sql_con
        print("Соединение успешно!")
        df1 = pd.read_sql("SELECT * FROM incidents", conn)
        print("Данные загружены успешно!")
        print(df1)
        return df1
    except e:
        print("Данные не выгружены:  " + str(e))

#

# функция выгрузки в excel из Дата фрейма
def df_to_excel(filename, df1):
    try:
        #con = sql_con(values[0]) 
        #df2 = df_upload(sql_con(values['-in1-']))
        df1.to_excel(filename, sheet_name='Выгрузка из БД')
        print("Информация записана в файл")
    except e:
        print("Ошибка выгрузки:  " + str(e))

# получить имя Таблицы из БД
def sql_get_table_name(conn):
    cur = conn.cursor()
    cur.execute('SELECT name from sqlite_master WHERE TYPE = "table"')
    tablename = cur.fetchall()
    print(tablename)
    #return tablename
# Для ТАБ 2
# функция для получения значений из полей ввода
def get_values(val1,val2,val3):
    # тестовый Принт - для проверки полученных значений
    print("первое: " + val1 + ", " + "второе: " + val2 + ", " + "третье: " + val3)

#Устанавливаем соединение с базой
def sql_conn():
	try:
		conn = sqlite3.connect("test_db1.db")
		return conn
		print('Соединение успешно!')
	except Error:
		print(Error)

# проверка и создание БД
def con_sql(connect):
    try:
        #conn = sqlite3.connect("test_db1.db")
        cur = connect.cursor()
# создание таблицы
        cur.execute("""CREATE TABLE IF NOT EXISTS incidents
                        (_id INTEGER PRIMARY KEY,
                        inc_num text, helper text, todo text)
                        """)
        print("БД успешно создана")
    except Error:
        print(Error)
    finally:
        connect.close()
        


# попытка внесения записи в Таблицу
def insert_sql(val1, val2, val3):
    # какая-то переменная
    #arr1 = [('первый','второй', 'третий')]
    arr_vals = [(val1, val2, val3)]
    #print (arr_vals)
    try:
        connect = sqlite3.connect("test_db1.db")    # " + val1 + " ," + val2 + " ," + val3 + "
        cur = connect.cursor()
        #cur.execute("""INSERT INTO incidents(inc_num, helper, todo) VALUES ( """ + val1 + " ," + val2 + " ," + val3 + " )""")
        cur.executemany("INSERT INTO incidents(inc_num, helper, todo) VALUES (?,?,? )", arr_vals)
        connect.commit() # без этой сраной строки ничего не запишется
        print("Данные успешно внесены")
    except Error:
        print(Error)
    finally:
        connect.close()

# показать записи в БД
def sql_select():
        try:
                connect = sqlite3.connect("test_db1.db")
                cur = connect.cursor()
                sql = "SELECT * FROM incidents"
                #sql1 = "SELECT * FROM artists WHERE name=?"
                cur.execute(sql)
                print(cur.fetchall())
                #cur.execute(sql1, [("Rammstein")])
                print(cur.fetchall())
        except Error:
                print(str(Error) + " не удалось показать")
        finally:
                cur.close()
#
#------------------------------- РИСУЕМ ОКНО СО СЛОЕМ, СОДЕРЖАЩИМ ТабГруп--------------------
window = psg.Window('Tabs', layout, no_titlebar=False)

# работа с событиями и значениями на форме
while True:
    events, values = window.read()
    path_db = ''
    path_excel = ''
    
    if events == psg.WIN_CLOSED:   #----------------------------------------
        break
    
    if events == 'btn_test1':
        conn = sql_con(values['-in1-'])
        sql_get_table_name(conn)
        """path_db = values['-folder-']
        path_excel = values['-in1-']
        if (path_db != '') and (path_excel != ''):
            psg.popup(' Всё на месте ', path_db + '  ' + path_excel )    #
        else:
            psg.popup('Не все значения присутствуют!!', path_db + '  ' + path_excel ) 
            print("Не все значения присутствуют!")
            print('----------------------------')"""
        #Esql_con(values['-in1-'])
        #text_input = values['-in1-']
        #psg.popup('Всё получилось успешно!', text_input)    #
        #print('----------------------------')
    if events == 'btn_from_db':
        #con = sql_con(values['-in1-'])
        df_upload(values['-in1-'], df1)
        window['btn_to_excel'].update(disabled=False)
        print('----------------------------')
    if events == 'btn_to_excel':
        path_db = values['-folder-']
        path_excel = values['-in1-']
        if (path_db != '') and (path_excel != ''):
                        # выгружает инфу из Бд и записывает в excel - chfpe dct jgthfwbb
            path_db = path_db + '/test1.xlsx'
            print(path_db)
            df_to_excel(path_db, df_upload(values['-in1-'], df1))
            print('----------------------------')
        else:
            psg.popup('Не все значения присутствуют!!', path_db + '  ' + path_excel ) 
            print("Не все значения присутствуют!")
            print('----------------------------')
    # -----------------------------------------------------------
    if events == 'sub':
        window['test1'].update(disabled=False)       
    elif events == 'test1':
        #print (values)
        #get_values(values[0], values[1], values[2])
        # заносим значения в массив, чтобы потом выгрузить в БД
        arr_vals = [values[0], values[1], values[2]]
        for i in arr_vals:
            print (i)
    elif events == 'test_db':
        connect = sql_conn()
        con_sql(connect)
    elif events == 'select_db':
        sql_select()
                            # проверка на заполнение полей - НЕ работает!!!
    elif events == 'insert_db':
        if not values:            
            print("Заполните все поля")
        elif values:
            
            connect = sql_conn()
            insert_sql(values[0], values[1], values[2])
            #insert_sql(connect)
            print("все поля заполнены")
